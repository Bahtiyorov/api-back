import bcrypt
import uuid

#print(await helpers.generate_hash('aiohttp-back-ohunjon'))

async def generate_hash(value):
    pas_bin=value.encode('utf-8')
    hashed=bcrypt.hashpw(pas_bin, bcrypt.gensalt())
    return hashed.decode('utf-8')

async def check_hash(plain_value, value_hash):
    plain_password_bin=plain_value.encode('utf-8')
    password_hash_bin=value_hash.encode('utf-8')
    is_correct=bcrypt.checkpw(plain_password_bin,password_hash_bin)
    return is_correct


async def validate_form(data):
    if data:
        count_error=0
        for field in data.values():
            #print(field)
            if field=='':
                count_error+=1
        if count_error>0:
            #print('bolshe nulya')
            return False
        else:
            #print('равно 0')
            return True
    else:
        return False


def gen_uuid():
    return  uuid.uuid1()






