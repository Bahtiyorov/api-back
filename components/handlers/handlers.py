from aiohttp import web
from components.db import check_user,reg_user, get_products,insert_token, get_token, get_index_images, get_category, search_result, get_product_by_id,del_product, upd_product,add_prod,get_user_by_token
from components.helpers import helpers
from components.constant import cons
import asyncio
import json


async def check_accses(request):
    if request.headers.get('auth_token') and request.headers.get('api_key'):
        if request.headers['api_key']==cons.api_key:
            auth_token=request.headers['auth_token']
            async with request.app['db'].acquire() as conn:
                token=await asyncio.shield(get_token(conn, auth_token))
                if token:
                    return True
                else:
                    return False
        else:
            return False
    else:
        return False


async def login(request):
    post=await asyncio.shield(request.json())
    try:
        if post:
            validate=await helpers.validate_form(post)
            if validate:
                email=post['email']
                password=post['password']
                async with request.app['db'].acquire() as conn:
                    check_u=await asyncio.shield(check_user(conn, email,password))
                    if check_u:
                        uuid=await asyncio.shield(insert_token(conn,check_u['id']))
                        if uuid:
                            #print(uuid)
                            return web.Response(headers={'Accept':'application/json',
                                                         'Contennt-Type':'application/json',
                                                         'auth_token':uuid},
                                                body=json.dumps({'user':check_u}),
                                                status=200)
                        else:
                            print('already authorizate')
                            return web.Response(text=json.dumps({'error':'You are already authorizate-you can use our service'}),status=401, content_type='application/json')


                    if not check_u:
                        print('user do not exists')
                        return web.Response(text=json.dumps({'error':'So user do not exists'}),status=401, content_type='application/json')
            else:
                print('fields is not be empty')
                return web.Response(text=json.dumps({'error':'Fields is not be empty'}), status=401, content_type='application/json')
    except Exception as e:
        print(e)
        res_obj={'error':str(e)}
        return web.Response(text=json.dumps(res_obj), status=500)


async def registration(request):
    post = await asyncio.shield(request.json())
    try:
        if post:
            validate=await helpers.validate_form(post)
            if validate:
                username=post['username']
                email=post['email']
                password=post['password']
                avatar='avatar.jpg'
                async with request.app['db'].acquire() as conn:
                    check_u=await asyncio.shield(check_user(conn, email,password))
                    if check_u:
                        return web.Response(text=json.dumps({'error':'So user already exists'}),status=401, content_type='application/json')
                    if not check_u:
                        registr=await asyncio.shield(reg_user(conn,username,email, password,avatar))
                        if registr:
                            uuid=await insert_token(conn,registr)
                            headers={'Accept':'application/json',
                                     'Contennt-Type':'application/json',
                                     'auth_token':uuid}
                            return web.Response(headers=headers, body=json.dumps({'user':'User created'}),status=201, content_type='application/json')
            else:
                print('fields is not be empty')
                return web.Response(text=json.dumps({'error': 'Fields is not be empty'}), status=401, content_type='application/json')

    except Exception as e:
        res_obj = {'error': str(e)}
        return web.Response(text=json.dumps(res_obj), status=500)


async def index(request):
    access=await check_accses(request)
    if access:
        try:
            async with request.app['db'].acquire() as conn:
                images=await asyncio.shield(get_index_images(conn))
                if images:
                    return web.Response(body=json.dumps({'images':images}), status=200, content_type='application/json')
        except Exception as e:
            print(e)
            res_obj = {'error': str(e)}
            return web.Response(text=json.dumps(res_obj), status=500)
    else:

        return web.Response(text=json.dumps({'error': 'You are not authorizaed! ha ha'}), status=401,
                             content_type='application/json')


async def products(request):
    accass=await check_accses(request)
    if accass:
        try:
            async with request.app['db'].acquire() as conn:
                product=await asyncio.shield(get_products(conn))
                category=await asyncio.shield(get_category(conn))
                #print(category)
            return web.Response(body=json.dumps({'products': product, 'category': category}), status=200, content_type='application/json')
        except Exception as e:
            print(e)
            res_obj = {'error': str(e)}
            return web.Response(text=json.dumps(res_obj), status=500)
    else:
        return web.Response(text=json.dumps({'error': 'You are not authorizaed! ha ha'}), status=401,
                 content_type='application/json')


async def search(request):
    accass = await check_accses(request)
    post=await asyncio.shield(request.json())
    if accass and post:
        try:
            #print('%' +post['search_key']+ '%')
            async with request.app['db'].acquire() as conn:
                result=await asyncio.shield(search_result(conn,post['search_key'], post['category']))
                #print(result)
            if result:
                return web.Response(body=json.dumps({'products': result}), status=200,
                            content_type='application/json')
            else:
                return web.Response(text=json.dumps({'products': 'Not found so product'}), status=404,
                                    content_type='application/json')
        except Exception as e:
            print(e)
            res_obj = {'error': str(e)}
            return web.Response(text=json.dumps(res_obj), status=500)
    else:
        return web.Response(text=json.dumps({'error': 'You are not authorizaed! ha ha'}), status=401,
                            content_type='application/json')


async def desc_product(request):
    accass = await check_accses(request)
    if accass:
        id=request.match_info['id']
        try:
            async with request.app['db'].acquire() as conn:
                product = await get_product_by_id(conn, id)
                category=await get_category(conn)

                if product and category:
                    return web.Response(body=json.dumps({'product':product, 'category':category}), status=200, content_type='application/json')
        except Exception as e:
            print(e)
    else:
        return web.Response(text=json.dumps({'error': 'You are not authorizaed! ha ha'}), status=401,
                            content_type='application/json')


async def user(request):
    access = await check_accses(request)
    if access:
        try:
            user_token=request.headers['auth_token']
            async with request.app['db'].acquire() as conn:
                user=await asyncio.shield(get_user_by_token(conn, user_token))
                if user:
                    #print(user)
                    return web.Response(body=json.dumps({'user': user}), status=200, content_type='application/json')
        except Exception as e:
            res_obj = {'error': str(e)}
            return web.Response(text=json.dumps(res_obj), status=500)
    else:
        return web.Response(text=json.dumps({'error': 'You are not authorizaed! ha ha'}), status=401,
                            content_type='application/json')


# api for admin

async def delete_product(request):
    access=await check_accses(request)
    id = request.match_info['id']
    if access:
        try:
            async with request.app['db'].acquire() as conn:
                result=await asyncio.shield(del_product(conn, id))
                if result:
                    return web.Response(body=json.dumps({'result': 'successfully deleted product'}), status=200, content_type='application/json')
        except Exception as e:
            res_obj = {'error': str(e)}
            return web.Response(text=json.dumps(res_obj), status=500)
    else:
        return web.Response(text=json.dumps({'error': 'You are not authorizaed! ha ha'}), status=401,
                            content_type='application/json')

async def update_product(request):
    access=await check_accses(request)
    post = await asyncio.shield(request.json())
    if access:
        try:
            async with request.app['db'].acquire() as conn:
                result=await asyncio.shield(upd_product(conn, post))
                if result:
                    return web.Response(body=json.dumps({'result': 'successfully updated product'}), status=200,
                                    content_type='application/json')
        except Exception as e:
            res_obj = {'error': str(e)}
            return web.Response(text=json.dumps(res_obj), status=500)
    else:
        return web.Response(text=json.dumps({'error': 'You are not authorizaed! ha ha'}), status=401,
                            content_type='application/json')

async def add_product(request):
    access = await check_accses(request)
    post=await asyncio.shield(request.json())
    if access:
        try:
            async with request.app['db'].acquire() as conn:
                result=await asyncio.shield(add_prod(conn, post))
                if result:
                    return web.Response(body=json.dumps({'result': 'successfully added new product'}), status=200,
                                content_type='application/json')
        except Exception as e:
            res_obj = {'error': str(e)}
            return web.Response(text=json.dumps(res_obj), status=500)
    else:
        return web.Response(text=json.dumps({'error': 'You are not authorizaed! ha ha'}), status=401,
                            content_type='application/json')

