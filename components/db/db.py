import asyncpgsa
from datetime import datetime as dt
from components.helpers import helpers
from sqlalchemy import (
MetaData, Table, Column, ForeignKey, Integer,
String, VARCHAR,DateTime
)
from sqlalchemy.sql import select

meta=MetaData()
#table users
users = Table(
    'users', meta,
    Column('id', Integer, primary_key=True),
    Column('username', VARCHAR(200), nullable=False),
    Column('email', VARCHAR(200), nullable=False),
    Column('password', VARCHAR(200), nullable=False),
    Column('avatar',VARCHAR(200),nullable=True),
    Column('timestamp', DateTime, default=dt.now())
)

#table category
category=Table(
    'category', meta,
    Column('id', Integer, primary_key=True),
    Column('name', VARCHAR(200), nullable=False)
)
#table products
products = Table(
    'products', meta,
    Column('id', Integer, primary_key=True),
    Column('category_id', Integer,
           ForeignKey('category.id', ondelete='CASCADE')
           ),
    Column('title', VARCHAR(200), nullable=True),
    Column('img', VARCHAR(200), nullable=True),
    Column('img_index', Integer),
    Column('description', String(1000),nullable=True)
)
#table tk_uuid -wich saved user_id and uuid this user
tk_uuid=Table(
    'tk_uuid',meta,
    Column('id', Integer, nullable=False),
    Column('uuid',VARCHAR(200), nullable=False),
    Column('time_auth',DateTime, default=dt.now())
)


#Подключение к бд
def constructor_db_url(conf):
        DSN = "postgresql://{user}:{password}@{host}:{port}/{database}"
        return DSN.format(
        database=conf['database'],
        user= conf['user'],
        password=conf['password'],
        host =conf['host'],
        port=conf['port'],
        minsize=conf['minsize'],
        maxsize=conf['maxsize']

        )

async def init_db(app):
    dsn=constructor_db_url(app['config']['postgres'])
    try:
        print('connect db')
        pool = await  asyncpgsa.create_pool(dsn=dsn)
        app['db']=pool
        return  pool
    except Exception as e:
        print(e)


async def close_db(app):
    print('close db')
    app['db'].close()
    app['db'].wait_close()



async def get_token(conn, token):
    result=await conn.fetchrow(tk_uuid.select().where(tk_uuid.c.uuid==token))
    if result:
        result=dict(result)
        return result
    return False

async def insert_token(conn,id):
    uuid= helpers.gen_uuid()
    uuid=str(uuid)
    #print(uuid)
    check_tk=await check_token(conn, uuid)
    if not check_tk:
        ins=tk_uuid.insert().values(id=id, uuid=uuid)
        result=await conn.execute(ins)
        if result:
            return uuid
        return False
    else:
        return False

async def check_token(conn,uuid):
    #print(uuid)
    result= await conn.fetchrow(tk_uuid.select().where(tk_uuid.c.uuid==uuid))
    if result:
        return True
    return False

async def get_user_by_id(conn, id):
    result= await conn.fetchrow(users.select().where(users.c.id==id))
    if result:
        res=dict(result)
        return res
    else:
        return None

async def get_user_by_token(conn, token):
    user=await get_token(conn,token)
    #print(user)
    if user:
        result=await get_user_by_id(conn, int(user['id']))
        if result:
            result['timestamp']=str(result['timestamp'])
            result=dict(result)
            return result
    else:
        return None

async def reg_user(conn, username, email,password, avatar):
    stmt= users.insert().values(username=username, email=email, password=password, avatar=avatar)
    reg=await conn.execute(stmt)
    if reg:
        user=await check_user(conn,email,password)
        #print(user['id'])
        return user['id']
    else:
        return False


async def check_user(conn, email, password):
    #print(email,'\n',password)
    result= await conn.fetchrow(users.select().where((users.c.email == email) & (users.c.password == password)))
    if result:
        result=dict(result)
        result['timestamp'] = str(result['timestamp'])
        #print(result)
        return result
    else:
        print('dont exists in db')
        return None

async def get_index_images(conn):
    try:
        result = await conn.fetch(
            select([products.c.img]).where(products.c.img_index==1)
        )
        rec=[]
        if result:
            for r in result:
                rec.append(dict(r))
                #print(rec)
            #print(rec)
            return rec
        else:
            return None
    except Exception as e:
        print(e)

async def get_products(conn):
    records = await conn.fetch(
        select([products]).order_by(products.c.id))
    ret=[]
    if records:
        for row in records:
            ret.append(dict(row))
        return ret
    else:
        return None

async def get_product_by_id(conn, id):
    id=int(id)
    records=await conn.fetchrow(select([products]).where(products.c.id==id))
    if records:
        records=dict(records)
        return records
    else:
        return None


async def get_category(conn):
    records = await conn.fetch(
        select([category]).order_by(category.c.id))
    ret = []
    if records:
        for row in records:
            ret.append(dict(row))
            # rec = dict(row)
            # print(rec)
        #print(ret)
        return ret
    else:
        return None

async def search_result(conn, search_key, category):
    #print(category)
    if category!='':
        category=int(category)
    #print(search_key,'\n',category)

    if search_key !='' and category == '':
        #print('s1')
        records= await  conn.fetch(select([products]).where(
            (products.c.title.like('%'+search_key+'%'))
            ).order_by(products.c.id))
    elif search_key =='' and category!='':
        #print('s2')
        records = await  conn.fetch(select([products]).where(
            (products.c.category_id==category)
        ).order_by(products.c.id))
    else:
        #print('s3')
        records = await  conn.fetch(select([products]).where(
            ((products.c.category_id == category)&(products.c.title.like('%'+search_key+'%')))
        ).order_by(products.c.id))


    res=[]
    if records:
        for row in records:
            res.append(dict(row))
        #print(res)
        return res
    else:
        return None

async def del_product(conn, id):
    if id:
        id=int(id)
    stmt=products.delete().where(products.c.id==id)
    d_prod=await conn.execute(stmt)
    if d_prod:
        return True
    else:
        False

async def upd_product(conn, product):
    #print(product)
    stmt=products.update().values(
        {
            products.c.category_id:int(product['category_id']),
            products.c.title:product['title'],
            products.c.img_index:int(product['img_index']),
            products.c.img:'avatar.png',
            products.c.description:product['description']
        }).where(products.c.id==int(product['id']))
    result=await conn.execute(stmt)
    if result:
        #print('true')
        return True
    else:
        return False

async def add_prod(conn, product):
    stmt = products.insert().values(category_id=int(product['category_id']),title=product['title'],
                                   img='back1.jpg', img_index=int(product['img_index']),description=product['description'])
    res = await conn.execute(stmt)
    if res:

        return True
    else:
        return False