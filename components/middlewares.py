from aiohttp import web
import json
@web.middleware
async def error_middleware(request, handler):
    try:
        response=await handler(request)
        if response.status !=404:
            return response
        message=response.message
    except web.HTTPException as ex:
        if ex.status != 404:
            raise
        message=ex.reason
    return web.Response(text=json.dumps({'error': message}),status=400, content_type='application/json')

def setup_middlewares(app):
    #error_middle=error_middleware()
    app.middlewares.append(error_middleware)