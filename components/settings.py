from pathlib import Path
import yaml
__all__ =('load_config')

def load_config(config_file=None):
    default_path=Path(__file__).parent/ 'config.yaml'
    #print(default_path)
    with open(default_path, 'r') as f:
        config =yaml.safe_load(f)
    cf_dict={}
    if config_file :
        cf_dict=yaml.safe_load(config_file)
    config.update(**cf_dict)

    return config
