from aiohttp import web
from components.routes import setup_routes
from components.middlewares import setup_middlewares
from components.db import init_db,close_db


async def create_app(config: dict):
    app=web.Application()
    app['config']=config

    setup_routes(app)
    setup_middlewares(app)

    app.on_startup.append(init_db)

    app.on_cleanup.append(close_db)


    return app