from components.handlers import handlers

def setup_routes(app):
    app.router.add_route('POST', r'/registration', handlers.registration, name='registration')
    app.router.add_route('POST', r'/login', handlers.login, name='login')
    app.router.add_route('GET', r'/index', handlers.index, name='index')
    app.router.add_route('GET',r'/products', handlers.products, name='products')
    app.router.add_route('POST', r'/search', handlers.search, name='search')
    app.router.add_route('GET', r'/desc/{id:\d+}', handlers.desc_product, name='desc_product')
    app.router.add_route('GET', r'/user', handlers.user, name='user')
    app.router.add_route('GET', r'/delete_product/{id:\d+}', handlers.delete_product, name='delete')
    app.router.add_route('POST', r'/update_product', handlers.update_product, name='update')

    app.router.add_route('POST', r'/add_product', handlers.add_product, name='add_product')




