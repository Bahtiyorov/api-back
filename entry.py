import aiohttp
import asyncio
import argparse
from components import  create_app
from components.settings import load_config


try:
    import uvloop
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
except ImportError:
    print('Library uvloop is not avilable')

parser = argparse.ArgumentParser(description="api-backend")
parser.add_argument('--host', help="Host to listen", default='127.0.0.1')
parser.add_argument('--port', help="Port to accept connection", default=5002)
parser.add_argument(
    '--reload',
    action="store_true",
    help="Autoreload code on change"
)


parser.add_argument("-c", "--config", type=argparse.FileType('r'),help="Path to configuration file")

args=parser.parse_args()

if args.reload:
    print('Start with code reload')
    import aioreloader
    aioreloader.start()

app=create_app(config=load_config(args.config))

if __name__ == '__main__':
    aiohttp.web.run_app(app, host=args.host, port=args.port)


#python3 ./entry.py --host=*
#https://ohunjon.nexustls.com/