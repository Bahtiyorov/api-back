from sqlalchemy import create_engine, MetaData
from components.db import constructor_db_url, users, category, products, tk_uuid
from components.settings import load_config

# psql -U postgres -h localhost
# CREATE DATABASE api_backend;
# CREATE USER api_user WITH PASSWORD '1996';
# GRANT ALL PRIVILEGES ON DATABASE api_backend TO api_user;

#create tables
def create_tables(engine):
    meta = MetaData()
    meta.create_all(bind=engine, tables=[users, category, products, tk_uuid])


#insert data to tables
def sample_data(engine):
    conn=engine.connect()
    conn.execute(category.insert(),[
        {'name': 'samsung'},
        {'name':'iphone'},
        {'name':'nokia'},
        {'name':'huawey'}
    ])

    conn.execute(products.insert(),[
        {'category_id':1,'title':'Galaxy 6','img':'back1.jpg','img_index':'1','description':'Its good phone'},
        {'category_id': 2, 'title': 'Galaxy 6', 'img': 'back1.jpg', 'img_index':'1','description': 'Its good phone qqqq'},
        {'category_id': 3, 'title': 'iphone 6', 'img': 'back1.jpg','img_index':'1', 'description': 'Its good phone eeee'},
        {'category_id': 4, 'title': 'nokia 1110', 'img': 'back1.jpg', 'img_index':'1','description': 'Its good phone rrrrr'},
        {'category_id': 1, 'title': 'Galaxy 9', 'img': 'back1.jpg', 'img_index':'1','description': 'Its good phone tttt'},
        {'category_id': 2, 'title': 'Iphone x', 'img': 'back1.jpg','img_index':'1', 'description': 'Its good phone ttttt'},

    ])
    conn.close()



config= load_config(None)
config=config['postgres']

if __name__ == '__main__':
    #print(config)
    db_url=constructor_db_url(config)
    engine=create_engine(db_url)
    #create_tables(engine)
    print('tables created')
    sample_data(engine)
    print('added data')
